<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use Mail;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
            //
    ];
    private $HEADER = array(
        'application/x-www-form-urlencoded'
    );
    private $URL = 'http://51.77.212.234:8282/api';

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function () {
            $emails = DB::select(' select * from emails where status=0 limit 10');
            foreach ($emails as $mail) {
                $email = $mail->email;
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $subject = $mail->subject;
                    $attachment = $mail->attachment;
                    $client = DB::table('client')->where('email', $mail->email)->first();
                    Mail::send('admin.email_template', ['content' => $mail->content, 'client' => $client], function ($m) use ($email, $subject, $attachment) {
                        $m->from('info@karibusms.com', 'karibuSMS');
                        $m->to($email)->subject($subject);
                        $attachment == null ? '' : $m->attach($attachment);
                    });
                    DB::table("update emails set status=1 where email='" . $mail->email . "'");
                } else {
                    DB::table('emails')->where('id', $mail->id)->delete();
                }
            }
        })->everyMinute();
             $schedule->call(function () {
            $messages = DB::select("SELECT a.content, a.phone_number,a.from_smart, a.pending_sms_id, a.username FROM pending_sms a JOIN client b ON a.client_id=b.client_id WHERE a.status='0'  and from_smart=0");
           
            foreach ($messages as $message) {
                 print_r($message);
                if ($message->from_smart == 0) {
                    $sender = new \SmsSender();
                    $sender->set_phone_number($message->phone_number);
                    $sender->set_message($message->content);
                    $sender->set_from_name($message->username);
                    $status = (object) json_decode($sender->send());
                    $delivered = $status->code == '1701' ? 'success' : 'pending';
                    $return_sms = $status->code . ' | ' . $status->message;
                    // } else {
//                $status = \Gcm::send($message->content, $message->phone_number, $message->username, $message->gcm_id);
//                $delivered = $status->success == 1 ? 'success' : 'pending';
//                $return_sms = '';
                    print_r($status);
                    DB::table('pending_sms')
                            ->where('pending_sms_id', $message->pending_sms_id)
                            ->update(
                                    [
                                        'status' => $status->success,
                                        'delivered_status' => $delivered,
                                        'return_message' => $return_sms
                                    ]
                    );
                }
            }
               })->everyMinute();

    
    }

    private function curl($fields) {
        // Open connection

        $ch = curl_init();
        // Set the url, number of POST vars, POST data

        curl_setopt($ch, CURLOPT_URL, $this->URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->HEADER);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function sendMessage($chatId, $text, $obj) {
        $data = array('chatId' => $chatId, 'body' => $text);
        $this->sendRequest('message', $data, $obj);
    }

    public function sendRequest($method, $data, $dev) {
        $url = $dev->api_secret . $method . '?token=' . $dev->api_key;
        if (is_array($data)) {
            $data = json_encode($data);
        }
        $options = stream_context_create(['http' => [
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => $data]]);

        $response = @file_get_contents($url, false, $options);
        // $response = $this->curlServer($body, $url);

        $requests = array('chat_id' => '43434', 'text' => $response, 'parse_mode' => '', 'source' => 'user');
        // file_put_contents('requests.log', $response . PHP_EOL, FILE_APPEND);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

}
