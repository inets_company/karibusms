/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  hp
 * Created: 01 Jul 2020
 */
 DROP TRIGGER after_delete_confirm ON new_karibusms.payment;

CREATE TRIGGER after_delete_confirm
    BEFORE DELETE
    ON new_karibusms.payment
    FOR EACH ROW
    EXECUTE PROCEDURE new_karibusms.no_delete_confirmed_payment();
