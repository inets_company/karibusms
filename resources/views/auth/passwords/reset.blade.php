@extends('master')

@section('content')
<h3 style="margin-top: 5em;"></h3>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>{{ __('Reset Password') }}</h2></div>

                @if ($message = request('m'))
                <div class="alert alert-top alert-success alert-dismissable margin5">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Success:</strong> {!! urldecode($message) !!}

                </div>
                @endif
                <?php if (isset($success_message)) { ?>
                    <div class="alert alert-top alert-success alert-dismissable margin5">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Success:</strong> {!! $success_message !!}

                    </div>
                <?php } ?>
                <?php if (isset($error_message)) { ?>
                    <div class="alert alert-top alert-danger alert-dismissable margin5">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error:</strong> {!! $error_message !!}

                    </div>
                <?php } ?>


                <div class="card-body">
                    <form method="POST" action="{{ url('password_reset_code/11') }}">
                        @csrf

                        <input type="hidden" name="token" id="token" value="{{ request('token') }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Enter Reset Codes</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="code" value="" required autocomplete="text" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  required autocomplete="new-password" minlength="6" maxlength="100">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" minlength="6" maxlength="100">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" id="check_password">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    check_password = function () {
        $('#check_password').mousedown(function () {
            var pass1 = $('#password').val();
            var pass2 = $('#password-confirm').val();
            if (pass1 == pass2) {
//                $.ajax({
//                    url: '<?= url('change_password') ?>',
//                    dataType: 'JSON',
//                    method: 'POST',
//                    data: {email: $('#email').val(), pas1: pass1, pas2: pass2, token: $('#token').val()},
//                    success: function (data) {
//                        $('#return').html(data.message).addClass('alert alert-' + data.status);
//                    }
//                })
            } else {
                alert('Password 1 and Password 2 does not match. Kindly use the same password');
                return false;
            }
        
        })
    }
    $(document).ready(check_password);
</script>
@endsection
